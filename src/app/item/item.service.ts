import { Injectable } from "@angular/core";

import { Item } from "./item";

@Injectable()
export class ItemService {
    private items = new Array<Item>(
        { id: 1, name: " Urgent some personal works", role: "3 Days" },
        { id: 3, name: "Urgent some personal works", role: "2 Days" },
        { id: 4, name: "Urgent some personal works", role: "4 Days" },
        { id: 5, name: "Urgent some personal works", role: "8 Days" },
        { id: 6, name: "Urgent some personal works", role: "2 Days" },
        { id: 7, name: "Urgent some personal works", role: "6 Days" },
        { id: 8, name: "Urgent some personal works", role: "5 Days" },
        { id: 9, name: "Urgent some personal works", role: "9 Days" },
        { id: 10, name: "Urgent some personal works", role: "12 Days" },
        { id: 11, name: "Urgent some personal works", role: "0.5 day" },
        { id: 12, name: "Urgent some personal works", role: "2 Days" },
        { id: 13, name: "Urgent some personal works", role: "5 Days" },
        { id: 14, name: "Urgent some personal works", role: "12 Days" },
        { id: 17, name: "Urgent some personal works", role: "9 Days" },
        { id: 18, name: "Urgent some personal works", role: "8 Days" },
        { id: 19, name: "Urgent some personal works", role: "7 Days" },
        { id: 20, name: "Urgent some personal works", role: "12 Days" },
        { id: 21, name: "Urgent some personal works", role: "15 Days" },
        { id: 22, name: "Urgent some personal works", role: "18 Days" },
        { id: 23, name: "Urgent some personal works", role: "19 Days" },
        { id: 24, name: "Urgent some personal works", role: "2 Days" },
        { id: 25, name: "Urgent some personal works", role: "5 Days" },
    );

    getItems(): Item[] {
        return this.items;
    }

    getItem(id: number): Item {
        return this.items.filter(item => item.id === id)[0];
    }
}
