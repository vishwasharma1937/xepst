"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./item.service");
var page_1 = require("tns-core-modules/ui/page/page");
var Toast = require("nativescript-toast");
var element_registry_1 = require("nativescript-angular/element-registry");
var nativescript_cardview_1 = require("nativescript-cardview");
element_registry_1.registerElement('CardView', function () { return nativescript_cardview_1.CardView; });
var ItemsComponent = /** @class */ (function () {
    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class.
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    function ItemsComponent(itemService, page) {
        this.itemService = itemService;
        this.page = page;
        this.approved = "Approved applications";
        page.actionBarHidden = true;
        page.backgroundSpanUnderStatusBar = true;
    }
    ItemsComponent.prototype.ngOnInit = function () {
        this.items = this.itemService.getItems();
    };
    ItemsComponent.prototype.onClickFab = function (args) {
        //alert("OnClickFab");
        Toast.makeText("Floating button clicked").show();
    };
    ItemsComponent.prototype.onApprovedClick = function (args) {
        //alert("Approved Clicked");
        Toast.makeText("Approved clicked").show();
        this.approved = "Approved applications";
        this.page.getViewById("approved").className = "enabledcolor";
        this.page.getViewById("applied").className = "disabledcolor";
        this.page.getViewById("approve").className = "enabledcolor";
        this.page.getViewById("applie").className = "disabledcolor";
    };
    ItemsComponent.prototype.onAppliedClicked = function (args) {
        //alert("Applied Clicked");
        Toast.makeText("Applied clicked").show();
        this.approved = "Applied applications";
        this.page.getViewById("approved").className = "disabledcolor";
        this.page.getViewById("applied").className = "enabledcolor";
        this.page.getViewById("approve").className = "disabledcolor";
        this.page.getViewById("applie").className = "enabledcolor";
    };
    ItemsComponent.prototype.onBellIconClicked = function () {
        Toast.makeText("Bell icon clicked").show();
    };
    ItemsComponent.prototype.onMenuclicked = function () {
        Toast.makeText("Menu icon clicked").show();
    };
    ItemsComponent.prototype.onScroll = function (event, scrollView, topView) {
        // If the header content is still visiible
        // console.log(scrollView.getViewById("tohidesection").className);
        // console.log(scrollView.verticalOffset);
        // console.log( this.page.getViewById("stick1"));
        // console.log( this.page.getViewById("stick1").className);
        // console.log(this.page.getViewById("tohidesection"));
        // console.log(this.page.getViewById("tohidesection").className);
        if (scrollView.verticalOffset > 200) {
            this.page.getViewById("stick1").className = "view3enable";
            this.page.getViewById("tohidesection").className = "view3disable";
            // scrollView.getViewById("secondstack").className = "view3enable";
            // scrollView.getViewById("topView").className = "view3disable";
        }
        else {
            this.page.getViewById("stick1").className = "view3disable";
            this.page.getViewById("tohidesection").className = "view3enable";
            // scrollView.getViewById("secondstack").className = "view3disable";
            // scrollView.getViewById("topView").className = "view3enable";
        }
    };
    ItemsComponent = __decorate([
        core_1.Component({
            selector: "ns-items",
            moduleId: module.id,
            styleUrls: ["./items.component.css"],
            templateUrl: "./items.component.html"
        }),
        __metadata("design:paramtypes", [item_service_1.ItemService, page_1.Page])
    ], ItemsComponent);
    return ItemsComponent;
}());
exports.ItemsComponent = ItemsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBU2xELCtDQUE2QztBQUM3QyxzREFBcUQ7QUFDckQsMENBQTRDO0FBQzVDLDBFQUF3RTtBQUN4RSwrREFBaUQ7QUFDakQsa0NBQWUsQ0FBQyxVQUFVLEVBQUUsY0FBTSxPQUFBLGdDQUFRLEVBQVIsQ0FBUSxDQUFDLENBQUM7QUFTNUM7SUFJSSw0SUFBNEk7SUFDNUksaUhBQWlIO0lBQ2pILHdCQUFvQixXQUF3QixFQUFVLElBQVU7UUFBNUMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBSnpELGFBQVEsR0FBVyx1QkFBdUIsQ0FBQztRQUs5QyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsNEJBQTRCLEdBQUcsSUFBSSxDQUFDO0lBQzdDLENBQUM7SUFFRCxpQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzdDLENBQUM7SUFFTSxtQ0FBVSxHQUFqQixVQUFrQixJQUFJO1FBQ2xCLHNCQUFzQjtRQUN0QixLQUFLLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFFckQsQ0FBQztJQUVNLHdDQUFlLEdBQXRCLFVBQXVCLElBQUk7UUFDdkIsNEJBQTRCO1FBQzVCLEtBQUssQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsUUFBUSxHQUFHLHVCQUF1QixDQUFDO1FBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsR0FBQyxjQUFjLENBQUM7UUFDM0QsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxHQUFDLGVBQWUsQ0FBQztRQUMzRCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLEdBQUMsY0FBYyxDQUFDO1FBQzFELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsR0FBQyxlQUFlLENBQUM7SUFFOUQsQ0FBQztJQUVNLHlDQUFnQixHQUF2QixVQUF3QixJQUFJO1FBQ3hCLDJCQUEyQjtRQUMzQixLQUFLLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxzQkFBc0IsQ0FBQztRQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLEdBQUMsZUFBZSxDQUFDO1FBQzVELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsR0FBQyxjQUFjLENBQUM7UUFDMUQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxHQUFDLGVBQWUsQ0FBQztRQUMzRCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLEdBQUMsY0FBYyxDQUFDO0lBRzdELENBQUM7SUFFTSwwQ0FBaUIsR0FBeEI7UUFDSSxLQUFLLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDL0MsQ0FBQztJQUVNLHNDQUFhLEdBQXBCO1FBQ0ksS0FBSyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFFRCxpQ0FBUSxHQUFSLFVBQVMsS0FBc0IsRUFBRSxVQUFzQixFQUFFLE9BQWE7UUFDbEUsMENBQTBDO1FBQzFDLGtFQUFrRTtRQUNsRSwwQ0FBMEM7UUFDMUMsaURBQWlEO1FBQ2pELDJEQUEyRDtRQUMzRCx1REFBdUQ7UUFDdkQsaUVBQWlFO1FBRWpFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLEdBQUcsYUFBYSxDQUFDO1lBQzFELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsR0FBRyxjQUFjLENBQUM7WUFDbEUsbUVBQW1FO1lBQ25FLGdFQUFnRTtRQUdwRSxDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDO1lBQzNELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUM7WUFDakUsb0VBQW9FO1lBQ3BFLCtEQUErRDtRQUVuRSxDQUFDO0lBQ0wsQ0FBQztJQTVFUSxjQUFjO1FBUDFCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDcEMsV0FBVyxFQUFFLHdCQUF3QjtTQUV4QyxDQUFDO3lDQU9tQywwQkFBVyxFQUFnQixXQUFJO09BTnZELGNBQWMsQ0E2RTFCO0lBQUQscUJBQUM7Q0FBQSxBQTdFRCxJQTZFQztBQTdFWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgU2Nyb2xsVmlldywgU2Nyb2xsRXZlbnREYXRhIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9zY3JvbGwtdmlldyc7XG5pbXBvcnQgeyBJbWFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvaW1hZ2UnO1xuaW1wb3J0IHsgVmlldyB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvY29yZS92aWV3JztcblxuaW1wb3J0IHsgc2NyZWVuIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy9wbGF0Zm9ybSc7XG5cbmltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9pdGVtXCI7XG5pbXBvcnQgeyBJdGVtU2VydmljZSB9IGZyb20gXCIuL2l0ZW0uc2VydmljZVwiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xuaW1wb3J0ICogYXMgVG9hc3QgZnJvbSAnbmF0aXZlc2NyaXB0LXRvYXN0JztcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnknO1xuaW1wb3J0IHsgQ2FyZFZpZXcgfSBmcm9tICduYXRpdmVzY3JpcHQtY2FyZHZpZXcnO1xucmVnaXN0ZXJFbGVtZW50KCdDYXJkVmlldycsICgpID0+IENhcmRWaWV3KTtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtaXRlbXNcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHN0eWxlVXJsczogW1wiLi9pdGVtcy5jb21wb25lbnQuY3NzXCJdLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vaXRlbXMuY29tcG9uZW50Lmh0bWxcIlxuXG59KVxuZXhwb3J0IGNsYXNzIEl0ZW1zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBpdGVtczogSXRlbVtdO1xuICAgIHB1YmxpYyBhcHByb3ZlZDogc3RyaW5nID0gXCJBcHByb3ZlZCBhcHBsaWNhdGlvbnNcIjtcblxuICAgIC8vIFRoaXMgcGF0dGVybiBtYWtlcyB1c2Ugb2YgQW5ndWxhcuKAmXMgZGVwZW5kZW5jeSBpbmplY3Rpb24gaW1wbGVtZW50YXRpb24gdG8gaW5qZWN0IGFuIGluc3RhbmNlIG9mIHRoZSBJdGVtU2VydmljZSBzZXJ2aWNlIGludG8gdGhpcyBjbGFzcy5cbiAgICAvLyBBbmd1bGFyIGtub3dzIGFib3V0IHRoaXMgc2VydmljZSBiZWNhdXNlIGl0IGlzIGluY2x1ZGVkIGluIHlvdXIgYXBw4oCZcyBtYWluIE5nTW9kdWxlLCBkZWZpbmVkIGluIGFwcC5tb2R1bGUudHMuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpdGVtU2VydmljZTogSXRlbVNlcnZpY2UsIHByaXZhdGUgcGFnZTogUGFnZSkge1xuICAgICAgICBwYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gICAgICAgIHBhZ2UuYmFja2dyb3VuZFNwYW5VbmRlclN0YXR1c0JhciA9IHRydWU7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuaXRlbXMgPSB0aGlzLml0ZW1TZXJ2aWNlLmdldEl0ZW1zKCk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uQ2xpY2tGYWIoYXJncykge1xuICAgICAgICAvL2FsZXJ0KFwiT25DbGlja0ZhYlwiKTtcbiAgICAgICAgVG9hc3QubWFrZVRleHQoXCJGbG9hdGluZyBidXR0b24gY2xpY2tlZFwiKS5zaG93KCk7XG5cbiAgICB9XG5cbiAgICBwdWJsaWMgb25BcHByb3ZlZENsaWNrKGFyZ3MpIHtcbiAgICAgICAgLy9hbGVydChcIkFwcHJvdmVkIENsaWNrZWRcIik7XG4gICAgICAgIFRvYXN0Lm1ha2VUZXh0KFwiQXBwcm92ZWQgY2xpY2tlZFwiKS5zaG93KCk7XG4gICAgICAgIHRoaXMuYXBwcm92ZWQgPSBcIkFwcHJvdmVkIGFwcGxpY2F0aW9uc1wiO1xuICAgICAgICB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJhcHByb3ZlZFwiKS5jbGFzc05hbWU9XCJlbmFibGVkY29sb3JcIjtcbiAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwiYXBwbGllZFwiKS5jbGFzc05hbWU9XCJkaXNhYmxlZGNvbG9yXCI7XG4gICAgICAgIHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFwcHJvdmVcIikuY2xhc3NOYW1lPVwiZW5hYmxlZGNvbG9yXCI7XG4gICAgICAgIHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFwcGxpZVwiKS5jbGFzc05hbWU9XCJkaXNhYmxlZGNvbG9yXCI7XG4gICAgICAgXG4gICAgfVxuXG4gICAgcHVibGljIG9uQXBwbGllZENsaWNrZWQoYXJncykge1xuICAgICAgICAvL2FsZXJ0KFwiQXBwbGllZCBDbGlja2VkXCIpO1xuICAgICAgICBUb2FzdC5tYWtlVGV4dChcIkFwcGxpZWQgY2xpY2tlZFwiKS5zaG93KCk7XG4gICAgICAgIHRoaXMuYXBwcm92ZWQgPSBcIkFwcGxpZWQgYXBwbGljYXRpb25zXCI7XG4gICAgICAgIHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFwcHJvdmVkXCIpLmNsYXNzTmFtZT1cImRpc2FibGVkY29sb3JcIjtcbiAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwiYXBwbGllZFwiKS5jbGFzc05hbWU9XCJlbmFibGVkY29sb3JcIjtcbiAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwiYXBwcm92ZVwiKS5jbGFzc05hbWU9XCJkaXNhYmxlZGNvbG9yXCI7XG4gICAgICAgIHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFwcGxpZVwiKS5jbGFzc05hbWU9XCJlbmFibGVkY29sb3JcIjtcblxuICAgICAgIFxuICAgIH1cblxuICAgIHB1YmxpYyBvbkJlbGxJY29uQ2xpY2tlZCgpIHtcbiAgICAgICAgVG9hc3QubWFrZVRleHQoXCJCZWxsIGljb24gY2xpY2tlZFwiKS5zaG93KCk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uTWVudWNsaWNrZWQoKSB7XG4gICAgICAgIFRvYXN0Lm1ha2VUZXh0KFwiTWVudSBpY29uIGNsaWNrZWRcIikuc2hvdygpO1xuICAgIH1cblxuICAgIG9uU2Nyb2xsKGV2ZW50OiBTY3JvbGxFdmVudERhdGEsIHNjcm9sbFZpZXc6IFNjcm9sbFZpZXcsIHRvcFZpZXc6IFZpZXcpIHtcbiAgICAgICAgLy8gSWYgdGhlIGhlYWRlciBjb250ZW50IGlzIHN0aWxsIHZpc2lpYmxlXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHNjcm9sbFZpZXcuZ2V0Vmlld0J5SWQoXCJ0b2hpZGVzZWN0aW9uXCIpLmNsYXNzTmFtZSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHNjcm9sbFZpZXcudmVydGljYWxPZmZzZXQpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyggdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwic3RpY2sxXCIpKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coIHRoaXMucGFnZS5nZXRWaWV3QnlJZChcInN0aWNrMVwiKS5jbGFzc05hbWUpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJ0b2hpZGVzZWN0aW9uXCIpKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5wYWdlLmdldFZpZXdCeUlkKFwidG9oaWRlc2VjdGlvblwiKS5jbGFzc05hbWUpO1xuXG4gICAgICAgIGlmIChzY3JvbGxWaWV3LnZlcnRpY2FsT2Zmc2V0ID4gMjAwKSB7XG4gICAgICAgICAgICB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJzdGljazFcIikuY2xhc3NOYW1lID0gXCJ2aWV3M2VuYWJsZVwiO1xuICAgICAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwidG9oaWRlc2VjdGlvblwiKS5jbGFzc05hbWUgPSBcInZpZXczZGlzYWJsZVwiO1xuICAgICAgICAgICAgLy8gc2Nyb2xsVmlldy5nZXRWaWV3QnlJZChcInNlY29uZHN0YWNrXCIpLmNsYXNzTmFtZSA9IFwidmlldzNlbmFibGVcIjtcbiAgICAgICAgICAgIC8vIHNjcm9sbFZpZXcuZ2V0Vmlld0J5SWQoXCJ0b3BWaWV3XCIpLmNsYXNzTmFtZSA9IFwidmlldzNkaXNhYmxlXCI7XG5cblxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwic3RpY2sxXCIpLmNsYXNzTmFtZSA9IFwidmlldzNkaXNhYmxlXCI7XG4gICAgICAgICAgICB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJ0b2hpZGVzZWN0aW9uXCIpLmNsYXNzTmFtZSA9IFwidmlldzNlbmFibGVcIjtcbiAgICAgICAgICAgIC8vIHNjcm9sbFZpZXcuZ2V0Vmlld0J5SWQoXCJzZWNvbmRzdGFja1wiKS5jbGFzc05hbWUgPSBcInZpZXczZGlzYWJsZVwiO1xuICAgICAgICAgICAgLy8gc2Nyb2xsVmlldy5nZXRWaWV3QnlJZChcInRvcFZpZXdcIikuY2xhc3NOYW1lID0gXCJ2aWV3M2VuYWJsZVwiO1xuXG4gICAgICAgIH1cbiAgICB9XG59Il19