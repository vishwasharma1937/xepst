import {Component} from "@angular/core"

@Component ({
    selector: "float-btn",
    moduleId: module.id,
    template:
        `
        <Stacklayout class="float-btn">
            <label class = "float-btn-text" text="+"></label>
        </Stacklayout> 
        `,
    styles: [ 
        `
        .float-btn {
            width: 58;
            height: 58;
            text-align: center;
            vertical-align: middle;
            border-width:4;
            border-color:#eaeaea;
            border-radius:100;
            background-color:#00955e
        }
        .float-btn-text {
            color: #fff;
            font-size: 34;
            margin-top: 2dp;
            text-align: center;
        }
        `
    ]    
})

export class FloatBtnComponent {

}