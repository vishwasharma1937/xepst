import { Component, OnInit } from "@angular/core";

import { ScrollView, ScrollEventData } from 'tns-core-modules/ui/scroll-view';
import { Image } from 'tns-core-modules/ui/image';
import { View } from 'tns-core-modules/ui/core/view';

import { screen } from 'tns-core-modules/platform';

import { Item } from "./item";
import { ItemService } from "./item.service";
import { Page } from "tns-core-modules/ui/page/page";
import * as Toast from 'nativescript-toast';
import { registerElement } from 'nativescript-angular/element-registry';
import { CardView } from 'nativescript-cardview';
registerElement('CardView', () => CardView);

@Component({
    selector: "ns-items",
    moduleId: module.id,
    styleUrls: ["./items.component.css"],
    templateUrl: "./items.component.html"

})
export class ItemsComponent implements OnInit {
    items: Item[];

    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class.
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    constructor(private itemService: ItemService, private page: Page) {
        page.actionBarHidden = true;
        page.backgroundSpanUnderStatusBar = true;
    }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    public onClickFab(args) {
        //alert("OnClickFab");
        Toast.makeText("Floating button clicked").show();

    }

    public onApprovedClick(){
        //alert("Approved Clicked");
        Toast.makeText("Approved clicked").show();
    }

    public onAppliedClicked(){
        //alert("Applied Clicked");
        Toast.makeText("Applied clicked").show();
    }

    public onBellIconClicked(){
        Toast.makeText("Bell icon clicked").show();
    }

    public onMenuclicked() {
        Toast.makeText("Menu icon clicked").show();
    }

    onScroll(event: ScrollEventData, scrollView: ScrollView, topView: View) {
        // If the header content is still visiible
        // console.log(scrollView.getViewById("tohidesection").className);
        // console.log(scrollView.verticalOffset);
        // console.log( this.page.getViewById("stick1"));
        // console.log( this.page.getViewById("stick1").className);
        // console.log(this.page.getViewById("tohidesection"));
        // console.log(this.page.getViewById("tohidesection").className);

        if (scrollView.verticalOffset > 200) {
            this.page.getViewById("stick1").className="view3enable";
            this.page.getViewById("tohidesection").className="view3disable";
            // scrollView.getViewById("secondstack").className = "view3enable";
            // scrollView.getViewById("topView").className = "view3disable";
            
            
        }
        else{
            this.page.getViewById("stick1").className="view3disable";
            this.page.getViewById("tohidesection").className="view3enable";
            // scrollView.getViewById("secondstack").className = "view3disable";
            // scrollView.getViewById("topView").className = "view3enable";
            
        }
    }
}