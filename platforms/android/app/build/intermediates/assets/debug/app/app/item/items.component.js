"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./item.service");
var page_1 = require("tns-core-modules/ui/page/page");
var Toast = require("nativescript-toast");
var element_registry_1 = require("nativescript-angular/element-registry");
var nativescript_cardview_1 = require("nativescript-cardview");
element_registry_1.registerElement('CardView', function () { return nativescript_cardview_1.CardView; });
var ItemsComponent = /** @class */ (function () {
    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class.
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    function ItemsComponent(itemService, page) {
        this.itemService = itemService;
        this.page = page;
        page.actionBarHidden = true;
        page.backgroundSpanUnderStatusBar = true;
    }
    ItemsComponent.prototype.ngOnInit = function () {
        this.items = this.itemService.getItems();
    };
    ItemsComponent.prototype.onClickFab = function (args) {
        //alert("OnClickFab");
        Toast.makeText("Floating button clicked").show();
    };
    ItemsComponent.prototype.onApprovedClick = function () {
        //alert("Approved Clicked");
        Toast.makeText("Approved clicked").show();
    };
    ItemsComponent.prototype.onAppliedClicked = function () {
        //alert("Applied Clicked");
        Toast.makeText("Applied clicked").show();
    };
    ItemsComponent.prototype.onBellIconClicked = function () {
        Toast.makeText("Bell icon clicked").show();
    };
    ItemsComponent.prototype.onMenuclicked = function () {
        Toast.makeText("Menu icon clicked").show();
    };
    ItemsComponent.prototype.onScroll = function (event, scrollView, topView) {
        // If the header content is still visiible
        // console.log(scrollView.getViewById("tohidesection").className);
        // console.log(scrollView.verticalOffset);
        // console.log( this.page.getViewById("stick1"));
        // console.log( this.page.getViewById("stick1").className);
        // console.log(this.page.getViewById("tohidesection"));
        // console.log(this.page.getViewById("tohidesection").className);
        if (scrollView.verticalOffset > 200) {
            this.page.getViewById("stick1").className = "view3enable";
            this.page.getViewById("tohidesection").className = "view3disable";
            // scrollView.getViewById("secondstack").className = "view3enable";
            // scrollView.getViewById("topView").className = "view3disable";
        }
        else {
            this.page.getViewById("stick1").className = "view3disable";
            this.page.getViewById("tohidesection").className = "view3enable";
            // scrollView.getViewById("secondstack").className = "view3disable";
            // scrollView.getViewById("topView").className = "view3enable";
        }
    };
    ItemsComponent = __decorate([
        core_1.Component({
            selector: "ns-items",
            moduleId: module.id,
            styleUrls: ["./items.component.css"],
            templateUrl: "./items.component.html"
        }),
        __metadata("design:paramtypes", [item_service_1.ItemService, page_1.Page])
    ], ItemsComponent);
    return ItemsComponent;
}());
exports.ItemsComponent = ItemsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBU2xELCtDQUE2QztBQUM3QyxzREFBcUQ7QUFDckQsMENBQTRDO0FBQzVDLDBFQUF3RTtBQUN4RSwrREFBaUQ7QUFDakQsa0NBQWUsQ0FBQyxVQUFVLEVBQUUsY0FBTSxPQUFBLGdDQUFRLEVBQVIsQ0FBUSxDQUFDLENBQUM7QUFTNUM7SUFHSSw0SUFBNEk7SUFDNUksaUhBQWlIO0lBQ2pILHdCQUFvQixXQUF3QixFQUFVLElBQVU7UUFBNUMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQzVELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUM7SUFDN0MsQ0FBQztJQUVELGlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDN0MsQ0FBQztJQUVNLG1DQUFVLEdBQWpCLFVBQWtCLElBQUk7UUFDbEIsc0JBQXNCO1FBQ3RCLEtBQUssQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUVyRCxDQUFDO0lBRU0sd0NBQWUsR0FBdEI7UUFDSSw0QkFBNEI7UUFDNUIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzlDLENBQUM7SUFFTSx5Q0FBZ0IsR0FBdkI7UUFDSSwyQkFBMkI7UUFDM0IsS0FBSyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzdDLENBQUM7SUFFTSwwQ0FBaUIsR0FBeEI7UUFDSSxLQUFLLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDL0MsQ0FBQztJQUVNLHNDQUFhLEdBQXBCO1FBQ0ksS0FBSyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFFRCxpQ0FBUSxHQUFSLFVBQVMsS0FBc0IsRUFBRSxVQUFzQixFQUFFLE9BQWE7UUFDbEUsMENBQTBDO1FBQzFDLGtFQUFrRTtRQUNsRSwwQ0FBMEM7UUFDMUMsaURBQWlEO1FBQ2pELDJEQUEyRDtRQUMzRCx1REFBdUQ7UUFDdkQsaUVBQWlFO1FBRWpFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLEdBQUMsYUFBYSxDQUFDO1lBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsR0FBQyxjQUFjLENBQUM7WUFDaEUsbUVBQW1FO1lBQ25FLGdFQUFnRTtRQUdwRSxDQUFDO1FBQ0QsSUFBSSxDQUFBLENBQUM7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLEdBQUMsY0FBYyxDQUFDO1lBQ3pELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsR0FBQyxhQUFhLENBQUM7WUFDL0Qsb0VBQW9FO1lBQ3BFLCtEQUErRDtRQUVuRSxDQUFDO0lBQ0wsQ0FBQztJQTlEUSxjQUFjO1FBUDFCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDcEMsV0FBVyxFQUFFLHdCQUF3QjtTQUV4QyxDQUFDO3lDQU1tQywwQkFBVyxFQUFnQixXQUFJO09BTHZELGNBQWMsQ0ErRDFCO0lBQUQscUJBQUM7Q0FBQSxBQS9ERCxJQStEQztBQS9EWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgU2Nyb2xsVmlldywgU2Nyb2xsRXZlbnREYXRhIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS9zY3JvbGwtdmlldyc7XG5pbXBvcnQgeyBJbWFnZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvaW1hZ2UnO1xuaW1wb3J0IHsgVmlldyB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvY29yZS92aWV3JztcblxuaW1wb3J0IHsgc2NyZWVuIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy9wbGF0Zm9ybSc7XG5cbmltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9pdGVtXCI7XG5pbXBvcnQgeyBJdGVtU2VydmljZSB9IGZyb20gXCIuL2l0ZW0uc2VydmljZVwiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xuaW1wb3J0ICogYXMgVG9hc3QgZnJvbSAnbmF0aXZlc2NyaXB0LXRvYXN0JztcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnknO1xuaW1wb3J0IHsgQ2FyZFZpZXcgfSBmcm9tICduYXRpdmVzY3JpcHQtY2FyZHZpZXcnO1xucmVnaXN0ZXJFbGVtZW50KCdDYXJkVmlldycsICgpID0+IENhcmRWaWV3KTtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtaXRlbXNcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHN0eWxlVXJsczogW1wiLi9pdGVtcy5jb21wb25lbnQuY3NzXCJdLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vaXRlbXMuY29tcG9uZW50Lmh0bWxcIlxuXG59KVxuZXhwb3J0IGNsYXNzIEl0ZW1zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBpdGVtczogSXRlbVtdO1xuXG4gICAgLy8gVGhpcyBwYXR0ZXJuIG1ha2VzIHVzZSBvZiBBbmd1bGFy4oCZcyBkZXBlbmRlbmN5IGluamVjdGlvbiBpbXBsZW1lbnRhdGlvbiB0byBpbmplY3QgYW4gaW5zdGFuY2Ugb2YgdGhlIEl0ZW1TZXJ2aWNlIHNlcnZpY2UgaW50byB0aGlzIGNsYXNzLlxuICAgIC8vIEFuZ3VsYXIga25vd3MgYWJvdXQgdGhpcyBzZXJ2aWNlIGJlY2F1c2UgaXQgaXMgaW5jbHVkZWQgaW4geW91ciBhcHDigJlzIG1haW4gTmdNb2R1bGUsIGRlZmluZWQgaW4gYXBwLm1vZHVsZS50cy5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGl0ZW1TZXJ2aWNlOiBJdGVtU2VydmljZSwgcHJpdmF0ZSBwYWdlOiBQYWdlKSB7XG4gICAgICAgIHBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcbiAgICAgICAgcGFnZS5iYWNrZ3JvdW5kU3BhblVuZGVyU3RhdHVzQmFyID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuaXRlbVNlcnZpY2UuZ2V0SXRlbXMoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25DbGlja0ZhYihhcmdzKSB7XG4gICAgICAgIC8vYWxlcnQoXCJPbkNsaWNrRmFiXCIpO1xuICAgICAgICBUb2FzdC5tYWtlVGV4dChcIkZsb2F0aW5nIGJ1dHRvbiBjbGlja2VkXCIpLnNob3coKTtcblxuICAgIH1cblxuICAgIHB1YmxpYyBvbkFwcHJvdmVkQ2xpY2soKXtcbiAgICAgICAgLy9hbGVydChcIkFwcHJvdmVkIENsaWNrZWRcIik7XG4gICAgICAgIFRvYXN0Lm1ha2VUZXh0KFwiQXBwcm92ZWQgY2xpY2tlZFwiKS5zaG93KCk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uQXBwbGllZENsaWNrZWQoKXtcbiAgICAgICAgLy9hbGVydChcIkFwcGxpZWQgQ2xpY2tlZFwiKTtcbiAgICAgICAgVG9hc3QubWFrZVRleHQoXCJBcHBsaWVkIGNsaWNrZWRcIikuc2hvdygpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbkJlbGxJY29uQ2xpY2tlZCgpe1xuICAgICAgICBUb2FzdC5tYWtlVGV4dChcIkJlbGwgaWNvbiBjbGlja2VkXCIpLnNob3coKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25NZW51Y2xpY2tlZCgpIHtcbiAgICAgICAgVG9hc3QubWFrZVRleHQoXCJNZW51IGljb24gY2xpY2tlZFwiKS5zaG93KCk7XG4gICAgfVxuXG4gICAgb25TY3JvbGwoZXZlbnQ6IFNjcm9sbEV2ZW50RGF0YSwgc2Nyb2xsVmlldzogU2Nyb2xsVmlldywgdG9wVmlldzogVmlldykge1xuICAgICAgICAvLyBJZiB0aGUgaGVhZGVyIGNvbnRlbnQgaXMgc3RpbGwgdmlzaWlibGVcbiAgICAgICAgLy8gY29uc29sZS5sb2coc2Nyb2xsVmlldy5nZXRWaWV3QnlJZChcInRvaGlkZXNlY3Rpb25cIikuY2xhc3NOYW1lKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coc2Nyb2xsVmlldy52ZXJ0aWNhbE9mZnNldCk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJzdGljazFcIikpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyggdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwic3RpY2sxXCIpLmNsYXNzTmFtZSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMucGFnZS5nZXRWaWV3QnlJZChcInRvaGlkZXNlY3Rpb25cIikpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJ0b2hpZGVzZWN0aW9uXCIpLmNsYXNzTmFtZSk7XG5cbiAgICAgICAgaWYgKHNjcm9sbFZpZXcudmVydGljYWxPZmZzZXQgPiAyMDApIHtcbiAgICAgICAgICAgIHRoaXMucGFnZS5nZXRWaWV3QnlJZChcInN0aWNrMVwiKS5jbGFzc05hbWU9XCJ2aWV3M2VuYWJsZVwiO1xuICAgICAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwidG9oaWRlc2VjdGlvblwiKS5jbGFzc05hbWU9XCJ2aWV3M2Rpc2FibGVcIjtcbiAgICAgICAgICAgIC8vIHNjcm9sbFZpZXcuZ2V0Vmlld0J5SWQoXCJzZWNvbmRzdGFja1wiKS5jbGFzc05hbWUgPSBcInZpZXczZW5hYmxlXCI7XG4gICAgICAgICAgICAvLyBzY3JvbGxWaWV3LmdldFZpZXdCeUlkKFwidG9wVmlld1wiKS5jbGFzc05hbWUgPSBcInZpZXczZGlzYWJsZVwiO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBlbHNle1xuICAgICAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwic3RpY2sxXCIpLmNsYXNzTmFtZT1cInZpZXczZGlzYWJsZVwiO1xuICAgICAgICAgICAgdGhpcy5wYWdlLmdldFZpZXdCeUlkKFwidG9oaWRlc2VjdGlvblwiKS5jbGFzc05hbWU9XCJ2aWV3M2VuYWJsZVwiO1xuICAgICAgICAgICAgLy8gc2Nyb2xsVmlldy5nZXRWaWV3QnlJZChcInNlY29uZHN0YWNrXCIpLmNsYXNzTmFtZSA9IFwidmlldzNkaXNhYmxlXCI7XG4gICAgICAgICAgICAvLyBzY3JvbGxWaWV3LmdldFZpZXdCeUlkKFwidG9wVmlld1wiKS5jbGFzc05hbWUgPSBcInZpZXczZW5hYmxlXCI7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIH1cbn0iXX0=