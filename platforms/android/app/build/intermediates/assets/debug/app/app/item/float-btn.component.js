"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var FloatBtnComponent = /** @class */ (function () {
    function FloatBtnComponent() {
    }
    FloatBtnComponent = __decorate([
        core_1.Component({
            selector: "float-btn",
            moduleId: module.id,
            template: "\n        <Stacklayout class=\"float-btn\">\n            <label class = \"float-btn-text\" text=\"+\"></label>\n        </Stacklayout> \n        ",
            styles: [
                "\n        .float-btn {\n            width: 58;\n            height: 58;\n            text-align: center;\n            vertical-align: middle;\n            border-width:4;\n            border-color:#eaeaea;\n            border-radius:100;\n            background-color:#00955e\n        }\n        .float-btn-text {\n            color: #fff;\n            font-size: 34;\n            margin-top: 2dp;\n            text-align: center;\n        }\n        "
            ]
        })
    ], FloatBtnComponent);
    return FloatBtnComponent;
}());
exports.FloatBtnComponent = FloatBtnComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmxvYXQtYnRuLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZsb2F0LWJ0bi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBdUM7QUFpQ3ZDO0lBQUE7SUFFQSxDQUFDO0lBRlksaUJBQWlCO1FBL0I3QixnQkFBUyxDQUFFO1lBQ1IsUUFBUSxFQUFFLFdBQVc7WUFDckIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFDSixtSkFJQztZQUNMLE1BQU0sRUFBRTtnQkFDSixxY0FpQkM7YUFDSjtTQUNKLENBQUM7T0FFVyxpQkFBaUIsQ0FFN0I7SUFBRCx3QkFBQztDQUFBLEFBRkQsSUFFQztBQUZZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiXHJcblxyXG5AQ29tcG9uZW50ICh7XHJcbiAgICBzZWxlY3RvcjogXCJmbG9hdC1idG5cIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZTpcclxuICAgICAgICBgXHJcbiAgICAgICAgPFN0YWNrbGF5b3V0IGNsYXNzPVwiZmxvYXQtYnRuXCI+XHJcbiAgICAgICAgICAgIDxsYWJlbCBjbGFzcyA9IFwiZmxvYXQtYnRuLXRleHRcIiB0ZXh0PVwiK1wiPjwvbGFiZWw+XHJcbiAgICAgICAgPC9TdGFja2xheW91dD4gXHJcbiAgICAgICAgYCxcclxuICAgIHN0eWxlczogWyBcclxuICAgICAgICBgXHJcbiAgICAgICAgLmZsb2F0LWJ0biB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1ODtcclxuICAgICAgICAgICAgaGVpZ2h0OiA1ODtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgICAgICAgICBib3JkZXItd2lkdGg6NDtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiNlYWVhZWE7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6MTAwO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiMwMDk1NWVcclxuICAgICAgICB9XHJcbiAgICAgICAgLmZsb2F0LWJ0bi10ZXh0IHtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzQ7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDJkcDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBgXHJcbiAgICBdICAgIFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEZsb2F0QnRuQ29tcG9uZW50IHtcclxuXHJcbn0iXX0=